import json
from difflib import get_close_matches


def get_detail(word):
  content = json.load(open('original.json'))
  if word.lower() in content or word.upper() in content or word.title() in content:
    return content[word]
  elif len(get_close_matches(word, content.keys())) > 0:
    new_word = get_close_matches(word, content.keys())[0]
    print('Would you write {} yes/no'.format(new_word))
    match = input()
    if match in 'yes':
      return content[new_word]
    else:
      return ['Not Found']
  else:
    return ['Not found']

while True:
  word = input("Enter a word: ")
  meaning = get_detail(word)
  if type(meaning) != None:
    for item in meaning:
      print(item)

  cont = input("Do you want to continue? yes/no: ")
  if cont == 'yes':
    continue
  else:
    print("Thank you!")
    break


