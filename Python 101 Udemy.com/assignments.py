# Q1. Write a for loop to print numbers 1 through 100 in ten rows.
for i in range(1, 101):
	print(i, end=" ")
	if i % 10 == 0:
		print("")

#==================================
# Q2. Write a for loop to print first 100 even numbers. Make sure the output is in 10 rows and format the output so that all columns align perfectly.

for i in range(2, 201, 2):
	print("{:>3}".format(i), end=" ")

	if i % 20 == 0:
		print(" ")

#==================================
"""
-----------------------------------------------------------------------------------------
Q3. Write a function isPrime(n), that returns a True if n is a prime number, if not False 
-----------------------------------------------------------------------------------------
"""
def isPrime(number):
	if number%2!=0 and number%3!=0 and number%5!=0 and number%7!=0:
		return True
	else:
		return False

print(isPrime(11))
print(isPrime(10))

#Q4. Write a for loop to print Prime numbers between 1 and 1000. Format the output so that all columns align perfectly
j = 0
for i in range(1, 1000):
	if i%2!=0 and i%3!=0 and i%5!=0 and i%7!=0:
		print("{:>3}".format(str(i)), end=" ")
		j+=1
	
		if j%10==0:
			print("")


# Q5. Write a function to convert temperature in celsius to fahrenheit
# Q6. Write a function to convert temperature in fahrenheit to celsius ( Q5 Reverse )
def cel2fah(cel):
	fah = (cel*1.8) + 32
	return fah

def fah2cel(fah):
	cel = (fah-32)/1.8
	return cel

print(cel2fah(0))
print(cel2fah(20))



# Q7. Convert the following list of Kilometers to miles.
[ 10, 20, 30, 40, 50 ]
Conversion: 1 Km = 0.621371 of a Mile.

km = [10, 20, 30, 40, 50]

for i in km:
	mile = i*0.621371
	print("{} Kilometers is equal to {} miles".format(i, mile))

# Q8. Write factorial function fact(n) without using recurrsion.

def fact(n):
	faktorial = 1

	for i in range(1, n+1):
		faktorial *= i

	return faktorial

print(fact(5))

# Q9. Write a factorial function factR(n) using recurrsion.
def factR(n):
	if( n < 1 ):
		print("Factorials do not exists for -ve numbers.")
		return
	if( n==1):
		return 1
	else:
		return n * factR(n-1)

print(factR(5))

# Q10. Write a function toBinary(n) to convert a decimal number into binary number using recurrsion
def toBinary(n):
	if n > 1:
		toBinary(n//2)
	print(n%2, end='')

toBinary(10)
print("")
toBinary(1234)


# Q11. Write your own mymap() function which works exactly like Python's built-in function map().
# mymap(fn, C) -- Takes in 2 parameters fn : a function and C: a contain
# er object.
# It applies the given function fn on each item in the container and pro
# duces a new
# container.

def mymap(fn, C):
	newC = []
	for x in C:
		newC.append(fn(x))
	return newC

A = [1,2,3,4]
print(mymap(lambda x: x+2, A))


# Q12. Write your own myreduce() function which works exactly like Python's built-in function reduce().
def myreduce(fn, A):
	reduceValue = 0
	for x in A:
		reduceValue = fn(reduceValue, x)
	return reduceValue

A = [1,2,33, 22, 11, 5]
print(myreduce(lambda x,y: x if x>y else y, A))

# Q13. Write your own myfilter() function which works exactly like Python's built-in function filter().
def myfilter(fn, C):
	newC = []
	for x in C:
		if fn(x):
			newC.append(x)
	return newC

A = [10, 2, 33, 22,11,5]
print(myfilter(lambda x: x%2==0, A))


# Q14. Write list comprehensions to produce the following three simple lists
# [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
# [1, 6, 11, 16, 21, 26, 31, 36, 41, 46]

list1 = [i for i in range(1, 11)]
list2 = [j for j in range(1, 20, 2)]
list3 = [z for z in range(1, 47, 5)]
print(list1)
print(list2)
print(list3)

# Q15. Write List comprehensions to produce the following Lists
# ['P', 'y', 't', 'h', 'o', 'n']
# ['x', 'xx', 'xxx', 'xxxx', 'y', 'yy', 'yyy', 'yyyy', 'z', 'zz', 'zzz', 'zzzz']
# ['x', 'y', 'z', 'xx', 'yy', 'zz', 'xx', 'yy', 'zz', 'xxxx', 'yyyy', 'zzzz']
# [[2], [3], [4], [3], [4], [5], [4], [5], [6]]
# [[2, 3, 4, 5], [3, 4, 5, 6], [4, 5, 6, 7], [5, 6, 7, 8]]
# [(1, 1), (2, 1), (3, 1), (1, 2), (2, 2), (3, 2), (1, 3), (2, 3), (3, 3)]
list1 = [i for i in "Python"]
print(list1)

list2 = [i*j for i in ('x', 'y', 'z') for j in range(1,5)]
print(list2)

list3 = [i*j*k for i in range(1,3) for j in range(1,3) for k in ('x', 'y', 'z')]
print(list3)

list4 = [ [i] for i in range(2,7)]
print(list4)

list5 = [[x+y for x in range(1, 5)] for y in range(1,5)]
print(list5)

list6 = [(x,y) for x in range(1,4) for y in range(1,4)]
print(list6)


# Q16. Write Dictionary comprehensions to produce the following Dictionaries:
# {1: 1, 2: 8, 3: 27, 4: 64, 5: 125, 6: 216, 7: 343, 8: 512, 9: 729}
#{'a': 'a', 'b': 'bb', 'c': 'ccc', 'd': 'dddd', 'e': 'eeeee', 'f': 'ffffff'}
my_dict = {x:x**3 for x in range(1, 10)}
print(my_dict)

possible = "abcdef"
my_dict2 = {y: y*(possible.index(y)+1) for y in possible}
print(my_dict2)


# Q17. Write a function longestWord() that takes a list of words and returns the longest one.
def longestWord(container):
	longest = 0
	longestWord = ""
	for i in container:
		if len(i) > longest:
			longest = len(i)
			longestWord = i

	return longestWord

print(longestWord(['Apple', 'Apricot', 'Asparagus', 'Avocado']))

# Q18. A palindrome is a word, phrase, number, or other sequence of characters which reads the
# same backward or forward. Allowances may be made for adjustments to capital letters,
# punctuation, and word dividers.

def palindrome(word):
	y = []
	for i in word:
		if i.isalnum():
			y.append(i.lower())

	if y == y[::1]:
		print("{}: is a Palindrome".format(word))
	else:
		print("{}: is not a Palindrome".format(word))

palindrome("noon")
palindrome("civic")
palindrome("racecar")
palindrome("kayak")
palindrome("Python")

palindrome("stack cats")
palindrome("step on no pets")
palindrome( "Was it a car or a cat I saw?")
palindrome("A man, a plan, a canal, Panama!")

# 19. A pangram is a sentence that contains all the letters of the English alphabetat least once.
# example: "The quick brown fox jumps over the lazy dog".
# Write a function to check if a sentence is a pangram or not
def is_panagram(sentence):
	allowed = "abcdefghijklmnopqstruvwxyz"
	check = {c: 0 for c in allowed}

	for i in sentence:
		if i.lower() == ' ':
			continue

		check[i.lower()] += 1

	for j in check:
		if check[j] == 0:
			print("It is not panagram")
			return

	print("It is a panagram")
	return

is_panagram( "The quick brown fox jumps over the lazy dog")

# 20. Write a function charFreq() that takes a string and creates a frequency listing of the characters
# contained in it using a dictionary object.
# charFreq("abcabcxyzxyzkkkmmmnnnsssqqqqkkaabbcc")

def charFreq(sentence):
	allowed = "abcdefghijklmnopqstruvwxyz"
	char_list = {c: 0 for c in allowed}

	for i in sentence:
		if i == ' ':
			continue
		char_list[i.lower()] += 1

	return char_list

print(charFreq("abcabcxyzxyzkkkmmmnnnsssqqqqkkaabbcc"))

# 21. Using random module, generate 10,000 numbers between 1 and 5. Find the number for which
# largest numbers generated. Also create a dictionary with key-values where value > 2000, using
# dictionary comprehensions.
# import random
# Use the method: random.randint(1, 5)

from random import randint

result = {c: 0 for c in range(1,6)}

for i in range(10000):
	number = randint(1,5)
	result[number] += 1

maximumValue = 0
maximumKey = 0

for i in result:
	if result[i] > maximumValue:
		maximumValue = result[i]
		maximumKey = i

print("Maximum Generated Number: {} ---> {} times".format(maximumKey, maximumValue))

new_dict = {x: y for x, y in result.items() if y > 2000}
print(new_dict)

