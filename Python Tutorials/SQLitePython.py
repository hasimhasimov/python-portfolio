import sqlite3
import datetime


with open("result.txt", "a") as result:

	print('Student Database')

	print("A -> Add, E->Edit, D -> Delete, R -> Read, Done -> Finish")

	try:
		connection = sqlite3.connect("C:\\Users\\hashi\\Desktop\\SQLiteStudio\\test")

		conn = connection.cursor()
	except:
		print("Can't connect to the database")


	def insert(full_name, gpa):
		try:
			insert_string = "INSERT INTO telebeler(full_name, gpa) VALUES('{}', '{}')".format(full_name, gpa)
			conn.execute(insert_string)
			connection.commit()
			result.write("{} students has been added to the database at the {} \n".format(full_name, datetime.datetime.now()))
			print("{} student has been added successfully".format(full_name))
		except:
			print("Database operations error.")


	while True:
		choice = input("Enter your command: ")
		if choice == 'A':
			print("Add new student:")
			full_name = input("Enter Full Name: ")
			try:
				gpa = float(input("Enter GPA: "))
			except ValueError as v:
				print("Enter valid data for GPA")
				continue
			else:
				insert(full_name, gpa)

		elif choice == 'R':
			print("THe list of students")
			for i in conn.execute("SELECT * FROM telebeler"):
				print("{}) {} - {}".format(i[0], i[1], i[2]))

			result.write("Students list has been read by user at {}\n".format(datetime.datetime.now()))

		elif choice == 'D':
			try:
				student = int(input("Which ID?: "))
			except ValueError as v:
				print("Enter valid data for ID")
				continue

			conn.execute("DELETE FROM telebeler WHERE id = '{}' ".format(student))
			connection.commit()
			result.write("{} ID student has been deleted at {}\n".format(student, datetime.datetime.now()))
			print("{} ID student has been Deleted successfully.".format(student))
		elif choice == 'E':
			try:
				student = int(input("Which ID?: "))
			except ValueError as v:
				print("Enter valid data for ID")
				continue

			try:	
				edit_item = conn.execute("SELECT * FROM telebeler WHERE id = '{}'".format(student)).fetchone()
				print("Edit: {}) {} {}".format(edit_item[0], edit_item[1], edit_item[2]))
			except:
				print("Database operations error")
				continue
			
			full_name = input("Enter full name (Remain blank for the same one): ")
			gpa = float(input("Enter GPA (0 for nothing): "))

			if full_name:
				try:
					conn.execute("UPDATE telebeler SET full_name = '{}' WHERE id = '{}'".format(full_name, student))
				except:
					print("Database operations error")
					continue
			
			if gpa != 0:
				try:
					conn.execute("UPDATE telebeler SET gpa = '{}' WHERE id = '{}'".format(gpa, student))
				except:
					print("Database operations error")
					continue

			connection.commit()
			result.write("{} student's data has been changed at {}\n".format(edit_item[1], datetime.datetime.now()))
			print("{} student's data has been changed successfully.".format(edit_item[1]))
		elif choice == 'Done':
			print("Good bye")
			result.write("Database operations has been finished at {}\n".format(datetime.datetime.now()))
			break